/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 01:10:33 by mstygg            #+#    #+#             */
/*   Updated: 2019/01/17 23:53:35 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <stdio.h>
#include "../includes/libft.h"

int _print_t(t_avl_t *tree, int is_left, int offset, int depth, char s[20][255]);
void print_t(t_avl_t *tree);

void preOrderTravers(t_avl_t* root) {
	if (root) 
	{
		printf("root!%d ", root->key);
		preOrderTravers(root->left);
		preOrderTravers(root->right);
	}
}
void	preorderprintbfactor(t_avl_t	*p)
{
	if (p)
	{
		printf("p->key = %d, bfacter = %d\n", p->key, ft_avl_bfactor(p));
		preorderprintbfactor(p->left);
		preorderprintbfactor(p->right);
	}
}
int main(void)
{
	t_avl_t* root = NULL;


	char c;
	int k;
	while ( scanf("%c", &c) && c != 'Q' )
	{
		if ( c == 'A' )
		{
			scanf("%d", &k);
			root = ft_avl_insert(root, k);
			printf(", root(insert) = %p,root->key = %d!!!\n",root, root->key);
		}
		else if ( c == 'S' )
		{
			scanf("%d", &k);
			t_avl_t* n = ft_avl_search(root, k);
			if ( n ) 
				printf("%d", n -> key);
			else
				printf("doesn't_exist\n");
		}
		else if(c == 'P')
			print_t(root);
		else if (c == 'D')
		{

			scanf("%d", &k);

		}
		else if(c == 'B')
		{
			preorderprintbfactor(root);
		}
		else if(c == 'M')
		{
			t_avl_t	*t1 = ft_avl_findmin(root);
			t_avl_t *t2 = ft_avl_findmin(root->right);
			printf("root_min = %d, root->right_min = %d\n", t1->key, t2->key);
		}
		else if(c == 'R')
		{
			scanf("%d", &k);
			root = ft_avl_remove(root, k);
		}
		/*
		else if ( c == 'D' )
		{
			scanf("%d", &k);
			root = remove_item(root, k);
		}
		*/
	}


//	free_tree(root);


	return 0;
}
